{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Authors**: Daniel Cox and Anton Såmark-Roth <br>\n",
    "Mail: <a href=\"mailto:daniel.cox@nuclear.lu.se\">daniel.cox@nuclear.lu.se</a> & <a href=\"mailto:anton.samark-roth@nuclear.lu.se\">anton.samark-roth@nuclear.lu.se</a>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Supplemental Material - Spectroscopic Tools Applied to Flerovium Decay Chains"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The preamplified signals of all channels in the Si-detectors were recorded using FEBEX3, 14 bit, 50 MHz sampling ADCs in 80 µs traces \\[[Ch. Lorenz, _et al._ Phys. Rev. C. **99**. 044310](https://journals.aps.org/prc/abstract/10.1103/PhysRevC.99.044310)\\]. \n",
    "In order to distinguish pile-up events created by, for example, two subsequent $\\alpha$-decays where the lifetime of the decay product is on the order of $\\sim$ µs or converted transitions of isomeric states in $\\alpha$-decay decay products, a digital system is essential.\n",
    "By employing various moving window deconvolution (MWD) algorithms it is possible to perform high-resolution particle spectroscopy.\n",
    "Moving window deconvolution routines have been developed to handle single-pulse traces and pile-up traces in previous work \\[[U. Forsberg, PhD thesis, 2016](https://portal.research.lu.se/portal/files/7495513/thesis.pdf), [A. Roth, Master thesis, 2016](https://lup.lub.lu.se/student-papers/search/publication/8882017), [A. Såmark-Roth, _et al._ Phys. Rev. C. **98**. 044307](https://journals.aps.org/prc/abstract/10.1103/PhysRevC.98.044307)\\]. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In the current work, the previous routines were extended to further optimise the pulse identification in pile-up signals.\n",
    "With the new routine it was possible to lower the energy limit for resolving pile-up signals down to 300 keV which can be compared to the previous pile-up routine which had a limit of about 900 keV.\n",
    "The new routine is a 2-step method which comprises the application of a 2nd-order low-pass Butterworth digital filter and an algorithm to identify bipolar pulses in the 2nd-derivative of the filtered signal.\n",
    "In this way, the routine is less sensitive to noise and still properly being able to identify pile-up pulses.\n",
    "While the new routine is robust in the sense that only a few parameters are needed, it is not as accomplished as the preceding pile-up routine in identifying pulses with a time difference of 10-50 samples (0.1 µs-0.5 µs).\n",
    "Our final routine, utilises the routine best fitted to the signal in question."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This notebook provides a step-by-step description of the routine with the implemented algorithm."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Import functions."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "import matplotlib.pyplot as plt\n",
    "from matplotlib import colors\n",
    "import matplotlib as mpl\n",
    "\n",
    "color_list = plt.rcParams['axes.prop_cycle'].by_key()['color'];\n",
    "\n",
    "from scipy import signal\n",
    "\n",
    "from IPython.core.display import display, HTML\n",
    "display(HTML(\"<style>.container { width:100% !important; }</style>\"))\n",
    "\n",
    "#%matplotlib widget"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Read example trace"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The trace as shown in the original article is read in and its baseline is restored."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "trace_example = {}\n",
    "trace_example[\"trace\"] = np.loadtxt(\"Example_trace.hdat\", dtype=int, skiprows=2, usecols=(3))\n",
    "trace_example[\"trace_blr\"] = np.subtract(trace_example[\"trace\"], np.mean(trace_example[\"trace\"][0:450]))\n",
    "samples = np.arange(0, 4000, 1)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "mpl.rcParams['font.size'] = 20\n",
    "#%matplotlib widget\n",
    "fig = plt.figure(figsize=(20,10))\n",
    "ax = fig.add_subplot(2, 1, 1)\n",
    "ax.plot(samples, trace_example[\"trace\"], color=color_list[0], alpha=0.75, label='Trace')\n",
    "ax.set_title('Example trace')\n",
    "ax.set_xlabel('Sample number')\n",
    "ax.set_ylabel('Energy (arb. unit)')\n",
    "plt.legend(loc='best')\n",
    "plt.grid(True)\n",
    "ax = fig.add_subplot(2, 1, 2)\n",
    "ax.plot(samples, trace_example[\"trace_blr\"], color=color_list[1], alpha=0.75, label='Trace, baseline restored')\n",
    "#ax.set_title('Trace, baseline restored')\n",
    "ax.set_xlabel('Sample number')\n",
    "ax.set_ylabel('Energy (arb. unit)')\n",
    "#ax.axis((10, 1000, -100, 10))\n",
    "ax.grid(which='both', axis='both')\n",
    "plt.legend(loc='best')\n",
    "plt.grid(True)\n",
    "\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Design a filter and view frequency and step response"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The following code presents the implemented digital low-pass Butterworth filter and its properties, frequency and step response.\n",
    "\n",
    "For an introduction to digital signal processing, we recommend: [The Scientist and Engineer's Guide to Digital Signal Processing Second Edition by Steven W. Smith](http://www.dspguide.com/).\n",
    "\n",
    "The `SCiPy.signal` module is used to generate filter parameters and apply the filter to the data. \n",
    "\n",
    "* Tutorial: https://docs.scipy.org/doc/scipy/reference/tutorial/signal.html\n",
    "* Reference guide: https://docs.scipy.org/doc/scipy/reference/signal.html"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "CUTOFF_FQ = 0.06\n",
    "ORDER = 2\n",
    "TYPE = 'butter'\n",
    "RIPPLE = 0.1 #only applicable to chebyshev\n",
    "\n",
    "# 2*CUTOFF_FQ corresponds to CUTOFF_FQ in Rob's c-code filters\n",
    "\n",
    "b, a = signal.iirfilter(ORDER, 2*CUTOFF_FQ, btype='lowpass', ftype=TYPE, output='ba')\n",
    "z, p, k = signal.iirfilter(ORDER, 2*np.pi*CUTOFF_FQ, btype='lowpass', ftype=TYPE, output='zpk')\n",
    "sos = signal.iirfilter(ORDER, 2*np.pi*CUTOFF_FQ, btype='lowpass', ftype=TYPE, output='sos')\n",
    "\n",
    "mpl.rcParams['font.size'] = 20\n",
    "#frequency response\n",
    "w, h = signal.freqs(b, a, 3000)\n",
    "fig = plt.figure(figsize=(20,5))\n",
    "ax = fig.add_subplot(1, 2, 1)\n",
    "ax.semilogx(w/(2*np.pi), 20*np.log10(abs(h)))\n",
    "ax.set_title('Frequency response (dB)')\n",
    "ax.set_xlabel('Frequency [samples/cycle]')\n",
    "ax.set_ylabel('Amplitude [dB]')\n",
    "#ax.axis((10, 1000, -100, 10))\n",
    "ax.grid(which='both', axis='both')\n",
    "\n",
    "ax = fig.add_subplot(1, 2, 2)\n",
    "ax.plot(w/(2*np.pi), (abs(h)))\n",
    "ax.set_title('Frequency response')\n",
    "ax.set_xlabel('Frequency [samples/cycle]')\n",
    "ax.set_ylabel('Amplitude')\n",
    "ax.set_xlim([0, 0.5])\n",
    "ax.grid(which='both', axis='both')\n",
    "plt.show()\n",
    "\n",
    "#impulse and step response\n",
    "step_x, step_y = signal.step2((b, a))\n",
    "\n",
    "fig = plt.figure(figsize=(20,5))\n",
    "ax = fig.add_subplot(1, 1, 1)\n",
    "ax.plot(step_x, step_y)\n",
    "ax.set_title('Step response')\n",
    "ax.set_xlabel('Samples')\n",
    "ax.set_ylabel('Amplitude')\n",
    "#ax.axis((10, 1000, -100, 10))\n",
    "ax.grid(which='both', axis='both')\n",
    "plt.show()\n",
    "\n",
    "print(\"Filter coefficients:\\n\")\n",
    "print(\"\\tnumerator, b=\", b, \"\\tdenominator, a=\", a, \"\\n\")\n",
    "print(\"\\tzeros, z=\", z, \"\\tpoles, p=\", p, \"\\tsystem gain, k=\", k, \"\\n\")\n",
    "print(\"\\tsecond order sections representation, sos=\", sos)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Apply filter to trace"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The implemented filter is applied to the example trace."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "trace_example[\"trace_blr_filt\"] = signal.filtfilt(b, a, trace_example[\"trace_blr\"])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Compute 2nd derivative of filtered trace"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The pulse identification procedure is based on the identification of bipolar pulses created by the 2nd derivative of the filtered signal. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "trace_example[\"trace_blr_filt_2ndderiv\"] = np.multiply(np.gradient(np.gradient(trace_example[\"trace_blr_filt\"])), 20) #multiplied by 20 to better distinguish this signal among the other signals"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Visualisation, filtered trace and 2nd derivative"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "mpl.rcParams['font.size'] = 20\n",
    "#%matplotlib widget\n",
    "fig = plt.figure(figsize=(20,10))\n",
    "ax = fig.add_subplot(1, 1, 1)\n",
    "ax.plot(samples, trace_example[\"trace_blr\"], color=color_list[1], alpha=0.75, label='Trace, baseline restored')\n",
    "ax.plot(samples, trace_example[\"trace_blr_filt\"], color=color_list[2], alpha=0.75, label='Trace, baseline restored, filtered')\n",
    "ax.plot(samples, trace_example[\"trace_blr_filt_2ndderiv\"], color=color_list[3], alpha=0.75, label='Trace, baseline restored, filtered, 2nd deriv')\n",
    "ax.set_xlim(450, 600)\n",
    "#ax.set_title('Trace, baseline restored')\n",
    "ax.set_xlabel('Sample number')\n",
    "ax.set_ylabel('Energy (arb. unit)')\n",
    "#ax.axis((10, 1000, -100, 10))\n",
    "ax.grid(which='both', axis='both')\n",
    "plt.legend(loc='best')\n",
    "plt.grid(True)\n",
    "\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Pulse identification - bipolar pulses"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The following routine has been translated from the C++ source code into this python function.\n",
    "\n",
    "Bipolar pulses are identified by the following logics: \n",
    "\n",
    "1. If the bipolar signal crosses a threshold, the maximum and minimum of the bipolar pulse is identified.\n",
    "2. Then if:\n",
    "    * the number of samples between the max and min is within [4, 11] samples and\n",
    "    * The ratio between the absolute value of max and min is within [0.8, 1.4]\n",
    "\n",
    "    -> the pulse is accepted"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "threshold = 30;\n",
    "\n",
    "def GetBipolarPulses(bipolar_signal, print_out = False):\n",
    "\n",
    "    deriv_change = -0.5;\n",
    "    length_gate = (4, 11);\n",
    "    amp_diff_ratio_gate = (0.8, 1.4);\n",
    "    deriv_at_change = [0, 0];\n",
    "    zero_crossing = 0;\n",
    "    nbr_pulses = 0\n",
    "    w_avg = 10\n",
    "    \n",
    "    v_t_rise, v_t_fall, v_timing, v_mov_avg = [], [], [], []\n",
    "\n",
    "    top_pulse_indicator = bottom_pulse_indicator = False;\n",
    "\n",
    "    bipolar_top, bipolar_bottom = [-1, -1], [-1, -1]\n",
    "\n",
    "    for i in range(len(bipolar_signal)): #skipping last 50 due to weird behaviour here with the filter\n",
    "        #reached bottom of bipolar pulse, storing values\n",
    "        if bottom_pulse_indicator and bipolar_signal[i]-bipolar_signal[i-1] > deriv_change:\n",
    "            bottom_pulse_indicator = False;\n",
    "            bipolar_bottom[0] = i-1;\n",
    "            bipolar_bottom[1] =  bipolar_signal[i-1];\n",
    "            deriv_at_change[1] =  bipolar_signal[i]- bipolar_signal[i-1];\n",
    "            if print_out:\n",
    "                print(\"\\tbipolar bottom; t={}, A={}\\n\".format(bipolar_bottom[0], bipolar_bottom[1]));\n",
    "\n",
    "            #check if bipolar pulse fulfills requirements for a proper pulse\n",
    "            length = bipolar_bottom[0] - bipolar_top[0];\n",
    "            amp_diff_ratio = abs(bipolar_top[1] / bipolar_bottom[1]);\n",
    "\n",
    "            amp_diff = bipolar_top[1] + abs(bipolar_bottom[1]); #add abs to avoid overshoots to become pulse!\n",
    "            if print_out:\n",
    "                print(\"\\tlength={}, amp_diff_ratio={}, amp_diff={}, deriv_at_change=[{}, {}]\\n\".format(length, amp_diff_ratio, amp_diff, deriv_at_change[0], deriv_at_change[1]));\n",
    "\n",
    "            if length >= length_gate[0] and length <= length_gate[1] and ( (amp_diff_ratio > amp_diff_ratio_gate[0] and amp_diff_ratio < amp_diff_ratio_gate[1]) or (bipolar_top[1] > 2.5*threshold and bipolar_bottom[1] < -2.5*threshold) ):\n",
    "                # stores pulse values\n",
    "                v_t_rise.append(bipolar_top[0]+1); #shift to align with FirstDerivTiming\n",
    "                v_t_fall.append(bipolar_bottom[0]+3); #shift to align with FirstDerivTiming\n",
    "                v_timing.append(zero_crossing+5);#shift to align\n",
    "                nbr_pulses += 1;\n",
    "\n",
    "                #print for debug\n",
    "                if print_out:\n",
    "                    print(\"\\t\\tPulse accepted!\\n\");\n",
    "\n",
    "            else:\n",
    "                if print_out:\n",
    "                    print(\"\\t\\tPulse NOT accepted!\\n\");\n",
    "\n",
    "        #determine zero-crossing for timing information, includes a 2-point linear interpolation\n",
    "        if bottom_pulse_indicator and bipolar_signal[i] < 0 and bipolar_signal[ i-1] > 0:\n",
    "            k =  bipolar_signal[ i] -  bipolar_signal[ i-1];\n",
    "            m =  bipolar_signal[ i] - k* i;\n",
    "            zero_crossing = -m / k;\n",
    "\n",
    "        #reached top of bipolar pulse, storing values\n",
    "        if top_pulse_indicator and bipolar_signal[i]- bipolar_signal[i-1] < deriv_change:\n",
    "            top_pulse_indicator = False;\n",
    "            bottom_pulse_indicator = True; #//now looking for bottom of bipolar pulse\n",
    "            bipolar_top[0] =  i-1;\n",
    "            bipolar_top[1] =  bipolar_signal[ i-1];\n",
    "            deriv_at_change[0] =  bipolar_signal[ i]- bipolar_signal[ i-1];\n",
    "            if print_out:\n",
    "                print(\"\\tbipolar top; t={}, A={}\\n\".format(bipolar_top[0], bipolar_top[1]))\n",
    "\n",
    "        #above threshold, now looking for top of bipolar pulse\n",
    "        if bipolar_signal[i] > threshold and not top_pulse_indicator and not bottom_pulse_indicator:\n",
    "            top_pulse_indicator = True;\n",
    "            if print_out:\n",
    "                print(\"\\tAbove threshold at: {}\\n\".format(i));\n",
    "        \n",
    "        #moving average \n",
    "        if i >= w_avg:\n",
    "            v_mov_avg.append(np.mean(bipolar_signal[i-w_avg:i]))\n",
    "        else: \n",
    "            v_mov_avg.append(0)\n",
    "            \n",
    "    if nbr_pulses == 1:\n",
    "        v_t_fall[0] += 28; #//shift to align with ShortTrapzTiming in case of single\n",
    "\n",
    "\n",
    "    if print_out: \n",
    "        for j in range(nbr_pulses):\n",
    "            print(\"Pulse #\", j+1)\n",
    "            print(\"\\t(t_rise, t_fall): (\",v_t_rise[j],\",\", v_t_fall[j],\")\")\n",
    "            print(\"\\ttiming: \", v_timing[j])\n",
    "\n",
    "    return v_t_rise, v_t_fall, v_timing, v_mov_avg"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The bipolar pulse identification routine is applied to the example trace and then visualised:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "v_t_rise, v_t_fall, v_timing, v_mov_avg = GetBipolarPulses(trace_example[\"trace_blr_filt_2ndderiv\"], print_out=True)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "mpl.rcParams['font.size'] = 20\n",
    "#%matplotlib widget\n",
    "fig = plt.figure(figsize=(20,10))\n",
    "ax = fig.add_subplot(2, 1, 1)\n",
    "ax.plot(samples, trace_example[\"trace_blr_filt_2ndderiv\"], color=color_list[3], alpha=0.75, label='Trace, baseline restored, filtered, 2nd deriv')\n",
    "#ax.plot(samples, v_mov_avg, color=color_list[5], alpha=0.75, label='Trace, baseline restored, filtered, 2nd deriv, mov avg')\n",
    "ax.axhline(threshold, linestyle='--', color='k', lw=2, label='Threshold')\n",
    "#ax.axhline(5, linestyle='--', color='b', lw=2)\n",
    "ax.set_xlim(450, 600)\n",
    "ax.set_title('Pulse identification - Example trace')\n",
    "ax.set_xlabel('Sample number')\n",
    "ax.set_ylabel('Energy (arb. unit)')\n",
    "#ax.axis((10, 1000, -100, 10))\n",
    "ax.grid(which='both', axis='both')\n",
    "plt.legend(loc='best')\n",
    "plt.grid(True)\n",
    "ax = fig.add_subplot(2, 1, 2)\n",
    "ax.plot(samples, trace_example[\"trace_blr\"], color=color_list[1], alpha=0.75, label='Trace, baseline restored')\n",
    "ax.plot(samples, trace_example[\"trace_blr_filt\"], color=color_list[2], alpha=0.75, label='Trace, baseline restored, filtered')\n",
    "for j in range(len(v_t_rise)):\n",
    "    if j == 0:\n",
    "        ax.plot(v_t_rise[j], trace_example[\"trace_blr\"][v_t_rise[j]], marker = '*', linestyle = '', color = 'r', markersize=12, label=\"t_rise\")\n",
    "        ax.plot(v_t_fall[j], trace_example[\"trace_blr\"][v_t_fall[j]], marker = '*', linestyle = '', color = 'k', markersize=12, label=\"t_fall\")\n",
    "    else:\n",
    "        ax.plot(v_t_rise[j], trace_example[\"trace_blr\"][v_t_rise[j]], marker = '*', linestyle = '', color = 'r', markersize=12)\n",
    "        ax.plot(v_t_fall[j], trace_example[\"trace_blr\"][v_t_fall[j]], marker = '*', linestyle = '', color = 'k', markersize=12)\n",
    "\n",
    "ax.set_xlim(450, 600)\n",
    "#ax.set_title('Trace, baseline restored')\n",
    "ax.set_xlabel('Sample number')\n",
    "ax.set_ylabel('Energy (arb. unit)')\n",
    "#ax.axis((10, 1000, -100, 10))\n",
    "ax.grid(which='both', axis='both')\n",
    "plt.legend(loc='best')\n",
    "plt.grid(True)\n",
    "\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Pile-up identification limitations"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Two properties of the pile-up traces, aggravate the identification of the pulses, namely:\n",
    "\n",
    "1. The pulse amplitude\n",
    "2. Time difference between the pulses\n",
    "\n",
    "Below these limitations are investigated with simulated traces for the developed routine."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "First the routine to simulate a signal is defined."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "TRACE_LEN = 4000\n",
    "BASELINE_START, BASELINE_END = 0, 450 #samples\n",
    "OFFSET = 2**13\n",
    "LEN_GAUSS_WIND, SIGMA_GAUSS_WIND = 8, 3 #samples\n",
    "\n",
    "tau = 650 # samples\n",
    "nbr_rise_samples = 3 #(includes start), note that Gaussian window smears it out further\n",
    "gauss_window = signal.gaussian(LEN_GAUSS_WIND, SIGMA_GAUSS_WIND)\n",
    "\n",
    "def sim_preamp_signal(t_in, A_in, noise_amplitude = 8, use_gauss_window=True):\n",
    "    noise = noise_amplitude*np.random.randn(TRACE_LEN)\n",
    "    noise += OFFSET\n",
    "    nbr_pulses = len(t_in)\n",
    "    trace = np.zeros(TRACE_LEN)\n",
    "    t_start = t_in[0]\n",
    "    if len(t_in) == 1:\n",
    "        t_end = samples[-1]\n",
    "    else:\n",
    "        t_end = t_in[1]\n",
    "    A = 0\n",
    "    for k in range(nbr_pulses):\n",
    "        A = A_in[k] + trace[t_start-1]\n",
    "        for r in range(0, nbr_rise_samples):\n",
    "            #print(\"trace, A_in: \", trace[t_start+r-1], A_in[k]/nbr_rise_samples)\n",
    "            trace[t_start+r] = trace[t_start+r-1] + A_in[k]/nbr_rise_samples\n",
    "        #add overshoot, ballistic deficit stuff\n",
    "        t_exp = np.array(range(0,int(t_end-t_start-nbr_rise_samples)))\n",
    "        exp_part = A*np.exp(-t_exp/tau)\n",
    "        trace[t_start+nbr_rise_samples:t_end] = exp_part\n",
    "        if k == nbr_pulses-1:\n",
    "            break\n",
    "        t_start = t_in[k+1]\n",
    "        if k == nbr_pulses-2:\n",
    "            t_end = samples[-1]\n",
    "        else:\n",
    "            t_end = t_in[k+2]\n",
    "    if use_gauss_window:\n",
    "        trace = signal.convolve(trace, gauss_window)[int(0.5*LEN_GAUSS_WIND):-int(0.5*LEN_GAUSS_WIND)+1]\n",
    "        trace = np.divide(trace, trace[t_in[0]+nbr_rise_samples+LEN_GAUSS_WIND]/A_in[0])\n",
    "    return trace+noise"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Determine noise band in simulated pulses based on the baseline in the example trace:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "noise_amplitude = np.std(trace_example[\"trace_blr\"][0:450])\n",
    "print(\"Standard deviation of baseline (noise amplitude) = \", noise_amplitude)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Make example simulated trace and run it through the pulse identification routine.\n",
    "\n",
    "**NOTE:** calibration factor is roughly 4!"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "t1 = 485\n",
    "t_diff = 50 # samples\n",
    "E1, E2 = 9400, 9000 # keV\n",
    "E1 *= 0.25\n",
    "E2 *= 0.25"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "trace_sim = {}\n",
    "trace_sim[\"trace\"] = sim_preamp_signal([t1, t1+t_diff], [E1, E2], noise_amplitude=noise_amplitude)\n",
    "trace_sim[\"trace_blr\"] = np.subtract(trace_sim[\"trace\"], np.mean(trace_sim[\"trace\"][0:450]))\n",
    "trace_sim[\"trace_blr_filt\"] = signal.filtfilt(b, a, trace_sim[\"trace_blr\"])\n",
    "trace_sim[\"trace_blr_filt_2ndderiv\"] = np.multiply(np.gradient(np.gradient(trace_sim[\"trace_blr_filt\"])), 20) #multiplied by 20 to better distinguish this signal among the other signals\n",
    "v_t_rise, v_t_fall, v_timing, v_mov_avg = GetBipolarPulses(trace_example[\"trace_blr_filt_2ndderiv\"], print_out=True)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "mpl.rcParams['font.size'] = 20\n",
    "#%matplotlib widget\n",
    "fig = plt.figure(figsize=(20,10))\n",
    "ax = fig.add_subplot(1, 1, 1)\n",
    "ax.plot(samples, trace_sim[\"trace_blr\"], color=color_list[1], alpha=0.75, label='Trace, baseline restored')\n",
    "ax.plot(samples, trace_sim[\"trace_blr_filt\"], color=color_list[2], alpha=0.75, label='Trace, baseline restored, filtered')\n",
    "ax.plot(samples, trace_sim[\"trace_blr_filt_2ndderiv\"], color=color_list[3], alpha=0.75, label='Trace, baseline restored, filtered, 2nd deriv')\n",
    "#ax.plot(samples, v_mov_avg, color=color_list[5], alpha=0.75, label='Trace, baseline restored, filtered, 2nd deriv, mov avg')\n",
    "ax.axhline(threshold, linestyle='--', color='k', lw=2, label='Threshold')\n",
    "ax.set_xlim(450, 600)\n",
    "ax.set_title('Trace, simulated')\n",
    "ax.set_xlabel('Sample number')\n",
    "ax.set_ylabel('Energy (arb. unit)')\n",
    "#ax.axis((10, 1000, -100, 10))\n",
    "ax.grid(which='both', axis='both')\n",
    "plt.legend(loc='best')\n",
    "plt.grid(True)\n",
    "\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "For simplicity the following class is implemented."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "class PulseID:\n",
    "    \n",
    "    def __init__(self, trace,  print_out = False):\n",
    "        self.trace = {}\n",
    "        self.trace[\"trace\"] = trace\n",
    "        self.trace[\"trace_blr\"] = np.subtract(self.trace[\"trace\"], np.mean(self.trace[\"trace\"][0:450]))\n",
    "        self.trace[\"trace_blr_filt\"] = signal.filtfilt(b, a, self.trace[\"trace_blr\"])\n",
    "        self.trace[\"trace_blr_filt_2ndderiv\"] = np.multiply(np.gradient(np.gradient(self.trace[\"trace_blr_filt\"])), 20) #multiplied by 20 to better distinguish this signal among the other signals\n",
    "        self.t_rise, self.t_fall, self.timing, self.mov_avg = GetBipolarPulses(self.trace[\"trace_blr_filt_2ndderiv\"], print_out)  "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Investigate pulse amplitude limitation"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Simulated signals with pulses of energies within a range from [20, 1000] keV is generated. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "t1 = 485\n",
    "E1 = np.linspace(20, 1000, 200) # keV\n",
    "E1 *= 0.25\n",
    "\n",
    "traces = []\n",
    "\n",
    "for i, e in enumerate(reversed(E1)):\n",
    "    traces.append(sim_preamp_signal([t1], [e], noise_amplitude=noise_amplitude))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Starting with the most energetic pulses, the pulse identification routine is applied and whenever it fails to identify the pulse in the trace, we break the loop."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "unidentified = []\n",
    "for i, e in enumerate(reversed(E1)):\n",
    "    pid = PulseID(traces[i])\n",
    "    if len(pid.t_rise) != 1:\n",
    "        print(\"Pulses identified: {} -> No longer identified at energy (amplitude) = {} keV ({})\".format(len(pid.t_rise), e*4, e))\n",
    "        unidentified.append((pid, e))\n",
    "        PulseID(traces[i], print_out=True)\n",
    "        break"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The smallest pulse amplitude that could be identified with the routine:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "for i, (pid, e) in enumerate(unidentified):\n",
    "    mpl.rcParams['font.size'] = 20\n",
    "    fig = plt.figure(figsize=(20,10))\n",
    "    ax = fig.add_subplot(1, 1, 1)\n",
    "    ax.plot(samples, pid.trace[\"trace_blr\"], color=color_list[1], alpha=0.75, label='Trace, baseline restored')\n",
    "    ax.plot(samples, pid.trace[\"trace_blr_filt\"], color=color_list[2], alpha=0.75, label='Trace, baseline restored, filtered')\n",
    "    ax.plot(samples, pid.trace[\"trace_blr_filt_2ndderiv\"], color=color_list[3], alpha=0.75, label='Trace, baseline restored, filtered, 2nd deriv')\n",
    "    ax.set_xlim(450, 600)\n",
    "    ax.axhline(threshold, linestyle='--', color='k', lw=2, label='Threshold')\n",
    "    ax.set_title('Energy (amplitude): {} keV ({})'.format(e*4,e))\n",
    "    ax.set_xlabel('Sample number')\n",
    "    ax.set_ylabel('Energy (arb. unit)')\n",
    "    #ax.axis((10, 1000, -100, 10))\n",
    "    ax.grid(which='both', axis='both')\n",
    "    plt.legend(loc='best')\n",
    "    plt.grid(True)\n",
    "    plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Investigate pile-up pulse time difference limitation"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Simulated pile-up signals with energies of 9 MeV with time differences ranging from [8, 200] samples ([0.16, 4.0] $\\mu$s) is generated. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "t1 = 485\n",
    "E1, E2 = 9000, 9000 # keV\n",
    "E1 *= 0.25\n",
    "E2 *= 0.25\n",
    "\n",
    "t_diff = np.arange(8, 200)\n",
    "\n",
    "traces = []\n",
    "\n",
    "for i, td in enumerate(reversed(t_diff)):\n",
    "    traces.append(sim_preamp_signal([t1, t1+td], [E1, E2], noise_amplitude=noise_amplitude))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Starting with the largest time difference, the pulse identification routine is applied and whenever it fails to identify 2 pulses in the trace, we break the loop."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "unidentified = []\n",
    "for i, td in enumerate(reversed(t_diff)):\n",
    "    pid = PulseID(traces[i])\n",
    "    if len(pid.t_rise) != 2:\n",
    "        print(\"Pulses identified: {} -> No longer identified at time diff = {} µs ({} samples)\".format(len(pid.t_rise), td*0.02, td))\n",
    "        unidentified.append((pid, td))\n",
    "        PulseID(traces[i], print_out=True)\n",
    "        break"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The smallest time difference that could be identified with the routine:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "for i, (pid, td) in enumerate(unidentified):\n",
    "    mpl.rcParams['font.size'] = 20\n",
    "    #%matplotlib widget\n",
    "    fig = plt.figure(figsize=(20,10))\n",
    "    ax = fig.add_subplot(2, 1, 1)\n",
    "    ax.plot(samples, pid.trace[\"trace_blr_filt_2ndderiv\"], color=color_list[3], alpha=0.75, label='Trace, baseline restored, filtered, 2nd deriv')\n",
    "    #ax.plot(samples, v_mov_avg, color=color_list[5], alpha=0.75, label='Trace, baseline restored, filtered, 2nd deriv, mov avg')\n",
    "    ax.axhline(threshold, linestyle='--', color='k', lw=2, label='Threshold')\n",
    "    #ax.axhline(5, linestyle='--', color='b', lw=2)\n",
    "    ax.set_xlim(450, 600)\n",
    "    ax.set_title(\"time diff = {} µs ({} samples)\".format(td*0.02, td))\n",
    "    ax.set_xlabel('Sample number')\n",
    "    ax.set_ylabel('Energy (arb. unit)')\n",
    "    #ax.axis((10, 1000, -100, 10))\n",
    "    ax.grid(which='both', axis='both')\n",
    "    plt.legend(loc='best')\n",
    "    plt.grid(True)\n",
    "    ax = fig.add_subplot(2, 1, 2)\n",
    "    ax.plot(samples, pid.trace[\"trace_blr\"], color=color_list[1], alpha=0.75, label='Trace, baseline restored')\n",
    "    ax.plot(samples, pid.trace[\"trace_blr_filt\"], color=color_list[2], alpha=0.75, label='Trace, baseline restored, filtered')\n",
    "    for j in range(len(pid.t_rise)):\n",
    "        if j == 0:\n",
    "            ax.plot(pid.t_rise[j], pid.trace[\"trace_blr\"][pid.t_rise[j]], marker = '*', linestyle = '', color = 'r', markersize=12, label=\"t_rise\")\n",
    "            ax.plot(pid.t_fall[j], pid.trace[\"trace_blr\"][pid.t_fall[j]], marker = '*', linestyle = '', color = 'k', markersize=12, label=\"t_fall\")\n",
    "        else:\n",
    "            ax.plot(pid.t_rise[j], pid.trace[\"trace_blr\"][pid.t_rise[j]], marker = '*', linestyle = '', color = 'r', markersize=12)\n",
    "            ax.plot(pid.t_fall[j], pid.trace[\"trace_blr\"][pid.t_fall[j]], marker = '*', linestyle = '', color = 'k', markersize=12)\n",
    "\n",
    "    ax.set_xlim(450, 600)\n",
    "    #ax.set_title('Trace, baseline restored')\n",
    "    ax.set_xlabel('Sample number')\n",
    "    ax.set_ylabel('Energy (arb. unit)')\n",
    "    #ax.axis((10, 1000, -100, 10))\n",
    "    ax.grid(which='both', axis='both')\n",
    "    plt.legend(loc='best')\n",
    "    plt.grid(True)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As can be seen in the figure above, the routine actually identifies 3 pulses.\n",
    "\n",
    "This is due to the under and overshoot of the filtered signal that aligns perfectly to disguise as a bipolar pulse.\n",
    "\n",
    "This scenario is extremely rare and the signals need to have a rather large amplitude.\n",
    "\n",
    "In the routine, the routine is not used if the time difference between the pulses is less than 45 samples!"
   ]
  }
 ],
 "metadata": {
  "hide_input": false,
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.1"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
